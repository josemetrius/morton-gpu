﻿using System.Linq;
using UnityEngine;
using System.Diagnostics;
using System.Collections;

public class Control: MonoBehaviour
{
	public float speed = 0.1f;

	private MeshRenderer render;
	private MeshFilter filter;

	void Start()
	{
		MeshFilter mf = GetComponentInChildren<MeshFilter>();
		MeshRenderer mr = GetComponentInChildren<MeshRenderer>();

		render = gameObject.AddComponent<MeshRenderer>();
		filter = gameObject.AddComponent<MeshFilter>();

		Vector3[] vertex = mf.mesh.vertices;
		int[] index = mf.mesh.GetIndices(0);

		Mesh noIdex = new Mesh();
		noIdex.name = "Mesh";
		noIdex.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
		noIdex.vertices = index.Select(i => vertex[i]).ToArray();
		noIdex.SetIndices(Enumerable.Range(0,index.Length).ToArray(),MeshTopology.Triangles,0);
		noIdex.UploadMeshData(false);

		mf.gameObject.SetActive(false);

		filter.sharedMesh = noIdex;
		render.sharedMaterial = mr.material;

		float total = ((float)(index.Length/3));
		render.sharedMaterial.SetFloat("_Total", total);
	}

	void Update()
	{
		transform.rotation = Quaternion.Euler(Vector3.up*(speed+transform.rotation.eulerAngles.y));

		if (Input.GetKeyUp(KeyCode.Space))
		{
			Mesh mesh = filter.sharedMesh;

			Vector3[] vertex = mesh.vertices;

			Stopwatch timer = new Stopwatch();
			timer.Start();
			MortonOrder.SortGPU(ref vertex);
			timer.Stop();
			UnityEngine.Debug.Log("Elapsed time for GPU: " + timer.ElapsedMilliseconds + " ms.");

			Mesh sorted = new Mesh();
			sorted.name = "sorted";
			sorted.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
			sorted.SetVertices(vertex);
			sorted.SetIndices(
				Enumerable.Range(0,vertex.Length).ToArray(),
				MeshTopology.Triangles,
				0
			);
			sorted.UploadMeshData(false);

			filter.sharedMesh = sorted;
		}
		if (Input.GetKeyUp(KeyCode.Q))
		{
			Mesh mesh = filter.sharedMesh;

			Vector3[] vertex = mesh.vertices;

			Stopwatch timer = new Stopwatch();
			timer.Start();
			MortonOrder.SortCPU(ref vertex);
			timer.Stop();
			UnityEngine.Debug.Log("Elapsed time for CPU: " + timer.ElapsedMilliseconds + " ms.");

			Mesh sorted = new Mesh();
			sorted.name = "sorted";
			sorted.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
			sorted.SetVertices(vertex);
			sorted.SetIndices(
				Enumerable.Range(0, vertex.Length).ToArray(),
				MeshTopology.Triangles,
				0
			);
			sorted.UploadMeshData(false);

			filter.sharedMesh = sorted;

		}
		if (Input.GetKeyUp(KeyCode.P))
		{
			StartCoroutine("GPU");
		}
		if (Input.GetKeyUp(KeyCode.O))
		{
			StartCoroutine("CPU");
		}

	}

	private Vector3[] Resize(Vector3[] src, int size)
	{
		int wrap = src.Length;

		Vector3[] temp = new Vector3[size*3];
		for(int i=0;i<size*3;++i)
		{
			temp[i] = src[i%wrap];
		}

		return temp;
	}

	IEnumerator GPU()
	{
		Mesh mesh = filter.sharedMesh;
		Vector3[] vertex = mesh.vertices;
		yield return null;

		int limit = 10000000;
		for(int i=10000;i<=limit;i*=10)
		{
			const int loops = 10;
			Vector3[] outer = Resize(vertex,i);
			yield return null;

			long mean = 0;
			long high = long.MinValue;
			long low = long.MaxValue;

			for(int j=0;j<loops;++j)
			{
				Vector3[] test = outer.ToArray();
				yield return null;

				Stopwatch timer = new Stopwatch();
				timer.Start();
				MortonOrder.SortGPU(ref test);
				timer.Stop();
				test = null;
				yield return null;

				long elapsed = timer.ElapsedMilliseconds;

				if(elapsed < low) low = elapsed;
				if(elapsed > high) high = elapsed;
					
				UnityEngine.Debug.Log(string.Format(
					"Elapsed {0}\n",
					elapsed.ToString()
				));
				mean += elapsed;
				yield return null;
			}

			mean /= loops;

			UnityEngine.Debug.Log(string.Format(
				"GPU Tri:{0}; {1}; {2}; {3};\n",
				i.ToString(),
				mean.ToString(),
				high.ToString(),
				low.ToString()
			));
			yield return null;
		}
	}
	IEnumerator CPU()
	{
		Mesh mesh = filter.sharedMesh;
		Vector3[] vertex = mesh.vertices;
		yield return null;

		int limit = 10000000;
		for(int i=10000;i<=limit;i*=10)
		{
			const int loops = 10;
			Vector3[] outer = Resize(vertex,i);
			yield return null;

			long mean = 0;
			long high = long.MinValue;
			long low = long.MaxValue;

			for(int j=0;j<loops;++j)
			{
				Vector3[] test = outer.ToArray();
				yield return null;

				Stopwatch timer = new Stopwatch();
				timer.Start();
				MortonOrder.SortCPU(ref test);
				timer.Stop();
				test = null;
				yield return null;

				long elapsed = timer.ElapsedMilliseconds;

				if(elapsed < low) low = elapsed;
				if(elapsed > high) high = elapsed;
					
				UnityEngine.Debug.Log(string.Format(
					"Elapsed {0}\n",
					elapsed.ToString()
				));
				mean += elapsed;
				yield return null;
			}

			mean /= loops;

			UnityEngine.Debug.Log(string.Format(
				"CPU Tri:{0}; {1}; {2}; {3};\n",
				i.ToString(),
				mean.ToString(),
				high.ToString(),
				low.ToString()
			));
			yield return null;
		}
	}
}
