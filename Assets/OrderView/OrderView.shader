﻿Shader "Unlit/OrderView"
{
	Properties
	{
		_Total("TraingleCount", Float) = 1000
	}

	CGINCLUDE
#include "UnityCG.cginc"

	float _Total;

	struct vin {
		uint id : SV_VertexID;
		float4 vertex : POSITION;
	};
	struct v2f {
		float4 pos : SV_POSITION;
		float4 color : COLOR;
	};

	v2f vert (vin v) {
		v2f o;

		uint tid = v.id/3;
		float order = frac(((float)tid)/_Total);

		o.pos = UnityObjectToClipPos(v.vertex);
		o.color = float4(order,order,order,1.0f);

		return o;
	}
	float4 frag (v2f i) : SV_Target { 
		return i.color;
	}
	ENDCG


	SubShader {
		Tags {
			"Queue"="Transparent"
			"RenderType"="Transparent"
		}
		Pass {
			ZWrite On

			CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
			ENDCG
		}
	}
}
