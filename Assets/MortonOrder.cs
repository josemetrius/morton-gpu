﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;


public static class MortonOrder
{
	private static ComputeShader compute;
		
	private static ComputeBuffer gpuMorton;
	private static ComputeBuffer gpuConf;
	private static ComputeBuffer gpuVertex;
	private static ComputeBuffer gpuBounds;

	private static int kernelBounds;
	private static int kernelTriangleMorton;
	private static int kernelTriangleRadix;


	private static void InitCompute(int count)
	{
		compute = (ComputeShader)Resources.Load("morton");

		kernelBounds = compute.FindKernel("BoundingBox");

		kernelTriangleMorton = compute.FindKernel("TriangleMorton");
		kernelTriangleRadix = compute.FindKernel("ParallelTriangleRadix");

		int vertCount = count*3;

		gpuMorton = new ComputeBuffer(count * 2, sizeof(int));
		gpuConf = new ComputeBuffer(1, sizeof(int));
		gpuVertex = new ComputeBuffer(vertCount * 2, 3 * sizeof(float));
		gpuBounds = new ComputeBuffer(2, 3 * sizeof(float));


		compute.SetBuffer(kernelBounds, "_Bounds", gpuBounds);
		compute.SetBuffer(kernelBounds, "_Vertex", gpuVertex);
		compute.SetBuffer(kernelBounds, "_Config", gpuConf);


		compute.SetBuffer(kernelTriangleMorton, "_Bounds", gpuBounds);
		compute.SetBuffer(kernelTriangleMorton, "_Vertex", gpuVertex);
		compute.SetBuffer(kernelTriangleMorton, "_Morton", gpuMorton);
		compute.SetBuffer(kernelTriangleMorton, "_Config", gpuConf);

		compute.SetBuffer(kernelTriangleRadix, "_Vertex", gpuVertex);
		compute.SetBuffer(kernelTriangleRadix, "_Morton", gpuMorton);
		compute.SetBuffer(kernelTriangleRadix, "_Config", gpuConf);
	}
	private static void ClearBuffers()
	{
		gpuMorton.Release();
		gpuConf.Release();
		gpuVertex.Release();
		gpuBounds.Release();

		gpuMorton = null;
		gpuConf = null;
		gpuVertex = null;
		gpuBounds = null;
	}

	public static void SortGPU(ref Vector3[] vertex)
	{
		int elementCount = vertex.Length/3;

		int[] config = new int[1];

		//Allocate an bind cumpute shaders and buffers
		InitCompute(elementCount);
		gpuVertex.SetData(vertex);


		//Bounds
		config[0] = vertex.Count();
		gpuConf.SetData(config);
		compute.Dispatch(kernelBounds, 1, 1, 1);


		//GPU Morton
		int job = elementCount / 32;
		if (elementCount % 32 != 0)
		{
			++job;
		}
		int y = job / 32768;
		if (job % 32768 != 0)
		{
			++y;
		}
		if (y > 1)
		{
			job = 32768;
		}
		config[0] = elementCount;
		gpuConf.SetData(config);

		compute.Dispatch(kernelTriangleMorton, job, y, 1);


		//GPU Radix
		compute.Dispatch(kernelTriangleRadix, 1, 1, 1);


		gpuVertex.GetData(vertex);

		ClearBuffers();
	}



	private static Vector3[] Normalize(Vector3[] vertex, Vector3 low, Vector3 high)
	{
		Vector3[] norm = new Vector3[vertex.Length];

		Vector3 invDim = new Vector3(
			1.0f/(high.x-low.x),
			1.0f/(high.y-low.y),
			1.0f/(high.z-low.z)
		);

		for (int i = 0;i < vertex.Length;++i)
		{
			norm[i] = vertex[i]-low;
			norm[i].Scale(invDim);
		}

		return norm;
	}
	private static int SpreadBit(int val)
	{
		val = (val | (val << 16)) & 0x030000FF;
		val = (val | (val <<  8)) & 0x0300F00F;
		val = (val | (val <<  4)) & 0x030C30C3;
		val = (val | (val <<  2)) & 0x09249249;

		return val;
	}
	private static int[] MortonCPU(Vector3[] vertex)
	{
		int[] morton = new int[vertex.Length];

		for (int i = 0;i < vertex.Length;++i)
		{
			if (i == 19395)
			{
				float vbs = 1023.0f;
				int bb = 10;
			}
			Vector3 v = vertex[i]*1023.0f;
			int xx = SpreadBit((int)v.x);
			int yy = SpreadBit((int)v.y);
			int zz = SpreadBit((int)v.z);

			morton[i] = xx | (yy << 1) | (zz << 2);
		}

		return morton;
	}

	public static void SortCPU(ref Vector3[] vertex)
	{
		 Vector3[] localVertex = vertex.ToArray();

		//calculate bounds
		Vector3 low = new Vector3(
			localVertex.Min(v => v.x),
			localVertex.Min(v => v.y),
			localVertex.Min(v => v.z)
		);
		Vector3 high = new Vector3(
			localVertex.Max(v => v.x),
			localVertex.Max(v => v.y),
			localVertex.Max(v => v.z)
		);

		//calculate center
		Vector3[] center = Enumerable.Range(0,localVertex.Length/3)
			.Select(i => (localVertex[i*3+0] + localVertex[i*3+1] + localVertex[i*3+2])/3.0f)
			.ToArray();

		//convert to 0 -> 1 range
		Vector3[] norm = Normalize(center, low, high);
		//SceneMesh.self.AddChild(norm.ToArray());

		//10 bit 3D morton code
		//morton = Morton1024(center);
		int[] morton = MortonCPU(norm);

		////Sorted order
		//int[] sorterOrder = Enumerable.Range(0, morton.Length).OrderBy(i => morton[i]).ToArray();
		//
		////Sort the vertices
		//vertex = sorterOrder.SelectMany(i => new[] { localVertex[i*3+0], localVertex[i*3+1], localVertex[i*3+2] }).ToArray();

		//Sort vertex and morton
		RadixIndex(ref morton, ref localVertex);

		vertex = localVertex;
	}


	[StructLayout(LayoutKind.Explicit)]
	private struct Union
	{
		[FieldOffset(0)] public int value;
		[FieldOffset(0)] public byte byte0;
		[FieldOffset(1)] public byte byte1;
		[FieldOffset(2)] public byte byte2;
		[FieldOffset(3)] public byte byte3;

		public static implicit operator Union(int rhs)
		{
			Union u = new Union();
			u.value = rhs;
			return u;
		}
		public static implicit operator int(Union rhs)
		{
			return rhs.value;
		}
	}
	public class DoubleBuffer<T>
	{
		int src=0;
		int dst=1;
		T[][] data;

		public void Flip()
		{
			dst = src;
			src = (src+1)%2;
		}

		public DoubleBuffer()
		{
		}
		public DoubleBuffer(T[] array)
		{
			data = new T[2][];
			data[0] = new T[array.Length];
			data[1] = new T[array.Length];

			Array.Copy(array,data[0],array.Length);
		}

		public void Front(ref T[] array)
		{
			Array.Copy(data[src],array,data[src].Length);
		}
		public void Front<U>(ref U[] array, Func<T,U> cast)
		{
			array = data[src].Select(e => cast(e)).ToArray();
		}

		public T this[int i]
		{
			get { return data[src][i]; }
			set { data[dst][i] = value; }
		}
		public static implicit operator DoubleBuffer<T>(T[] rhs)
		{
			return new DoubleBuffer<T>(rhs);
		}

	}

	public static void RadixIndex(ref int[] morton, ref Vector3[] vertex)
	{
		Union[] arr = morton.Select(m => (Union)m).ToArray();
		DoubleBuffer<Union> comp = arr;
		DoubleBuffer<Vector3> vert = vertex;

		int[] histogram = Enumerable.Repeat(0,256*4).ToArray();
		int[] offset = Enumerable.Repeat(0,256*4).ToArray();

		int bufLen = morton.Length;

		//counting
		for (int i = 0;i<bufLen;++i)
		{
			++histogram[comp[i].byte0 + 256*0];
			++histogram[comp[i].byte1 + 256*1];
			++histogram[comp[i].byte2 + 256*2];
			++histogram[comp[i].byte3 + 256*3];
		}
		//offset
		for (int i = 1;i<256;++i)
		{
			offset[i+256*0] = offset[i-1+256*0]+histogram[i-1+256*0];
			offset[i+256*1] = offset[i-1+256*1]+histogram[i-1+256*1];
			offset[i+256*2] = offset[i-1+256*2]+histogram[i-1+256*2];
			offset[i+256*3] = offset[i-1+256*3]+histogram[i-1+256*3];
		}
		//radix sort
		//byte0
		for (int j = 0;j<bufLen;++j)
		{
			int off = offset[comp[j].byte0 + 256*0];
			++offset[comp[j].byte0 + 256*0];

			comp[off] = comp[j];
			vert[off*3+0] = vert[j*3+0];
			vert[off*3+1] = vert[j*3+1];
			vert[off*3+2] = vert[j*3+2];
		}
		comp.Flip();
		vert.Flip();
		//byte1
		for (int j = 0;j<bufLen;++j)
		{
			int off = offset[comp[j].byte1 + 256*1];
			++offset[comp[j].byte1 + 256*1];

			comp[off] = comp[j];
			vert[off*3+0] = vert[j*3+0];
			vert[off*3+1] = vert[j*3+1];
			vert[off*3+2] = vert[j*3+2];
		}
		comp.Flip();
		vert.Flip();
		//byte2
		for (int j = 0;j<bufLen;++j)
		{
			int off = offset[comp[j].byte2 + 256*2];
			++offset[comp[j].byte2 + 256*2];

			comp[off] = comp[j];
			vert[off*3+0] = vert[j*3+0];
			vert[off*3+1] = vert[j*3+1];
			vert[off*3+2] = vert[j*3+2];
		}
		comp.Flip();
		vert.Flip();
		//byte3
		for (int j = 0;j<bufLen;++j)
		{
			int off = offset[comp[j].byte3 + 256*3];
			++offset[comp[j].byte3 + 256*3];

			comp[off] = comp[j];
			vert[off*3+0] = vert[j*3+0];
			vert[off*3+1] = vert[j*3+1];
			vert[off*3+2] = vert[j*3+2];
		}
		comp.Flip();
		vert.Flip();

		comp.Front(ref morton, u => u.value);
		vert.Front(ref vertex);
	}
}
